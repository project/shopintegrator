CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Installation

INTRODUCTION
------------

This module provides integration with the 3rd party hosted e-commerce service
ShopIntegrator.

Further reading: http://www.shopintegrator.com

FEATURES
--------

- Choice of field formatters to sell products.
- Basket summary Block.
- View/Basket Checkout Block.

INSTALLATION
------------

1. Download and install the ShopIntegrator module into your respective sites
directory (sites/all/modules).

2. Enable the module in admin/build/modules.

3. Visit admin/config/shopintegrator/settings and enter your ShopIntegrator
client code.

4. Create a new custom text field with a maximum length of 20 characters to
store the ShopIntegrator Stock Code ID.

5. Enable the text field formatter in your display or view.
