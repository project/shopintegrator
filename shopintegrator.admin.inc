<?php

/**
 * @file
 * Administrative page callbacks for the shopintegrator module.
 */


/**
 * Implements hook_admin_settings().
 */
function shopintegrator_admin_settings_form($form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['account']['shopintegrator_id'] = array(
    '#title' => t('Client Code'),
    '#type' => 'textfield',
    '#default_value' => variable_get('shopintegrator_id', ''),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('Please enter your Shop Integrator Client Code'),
  );

  return system_settings_form($form);
}


/**
 * Implements _form_validate().
 */
function shopintegrator_admin_settings_form_validate($form, &$form_state) {

  // Trim text values.
  $form_state['values']['shopintegrator_account'] = trim($form_state['values']['shopintegrator_id']);

}

/**
 * Menu callback; Provide the administration overview page.
 */
function shopintegrator_admin_config_page() {
  $output = '<p>' . t("The following link takes you to the external shop integrator admin page. You will need your Shop Integrator username and password to gain access to the admin page.") . '</p>';
  $output .= '<p>' . l(t('Shop Integrator Admin'), 'http://admin.shopintegrator.com', array('attributes' => array('target' => array('_blank')))) . '</p>';
  $output .= '<p>' . t("The above link will open in a new window, close the window to return to this page.");
  return $output;
}
